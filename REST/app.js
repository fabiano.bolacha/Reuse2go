const express    = require('express');
const app        = express();
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');

const rotaUser        = require('./routes/user'             );
const rotaBorrow      = require('./routes/borrow'           );
const rotaGruopUser   = require('./routes/gruop_user'       );
const rotaCompany     = require('./routes/company'          );
const rotaUserCompany = require('./routes/users_has_company');
const swaggerDocument = require('./swagger.json');

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.use( (req, res, next) => {
    res.header('Acces-Control-Allow-Origin', '*');
    res.header(
        'Acces-Control-Allow-Header', 
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

    if( req.method === 'OPTIONS' ){
        res.header('Access-Control-Allow-Methods','PUT, POST, PATCH, DELETE,GET');
        return res.status(200).send({});
    }
    next();

});

app.use('/user'             , rotaUser       );
app.use('/borrow'           , rotaBorrow     );
app.use('/gruop_user'       , rotaGruopUser  );
app.use('/company'          , rotaCompany    );
app.use('/users_has_company', rotaUserCompany);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use( (req, res, next) => {
   const erro = new Error('Not found');
   erro.status = 404;
   next(erro);
});

module.exports = app;