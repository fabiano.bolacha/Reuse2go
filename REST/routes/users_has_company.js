const express = require('express');
const router = express.Router();
const login = require('../middleware/login');

const UserCompanyConttoller = require('../controllers/users_has_company-controller');

// Inseri usuario no grupo padrão.
router.post('/', login.mandatory, UserCompanyConttoller.postUserCompany );

module.exports = router;