const express = require('express');
const router = express.Router();
const login = require('../middleware/login');

const CompanyConttoller = require('../controllers/company-controller');

// Pesquisa company.
router.get('/', login.mandatory, CompanyConttoller.getCompany );
// Retornar um company
router.get('/:id_company', login.mandatory, CompanyConttoller.getCompanyDetail);

//router.patch('/:company',  )

module.exports = router;