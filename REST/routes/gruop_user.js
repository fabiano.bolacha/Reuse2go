const express = require('express');
const router = express.Router();
const login = require('../middleware/login');

const GroupUserConttoller = require('../controllers/gruop_user-controller');

// Inseri usuario no grupo padrão.
router.post('/', login.mandatory, GroupUserConttoller.postGroupUser );

module.exports = router;