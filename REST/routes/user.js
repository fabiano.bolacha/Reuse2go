const express = require('express');
const router = express.Router();
const login = require('../middleware/login');
const multer = require('multer');

const UserConttoller = require('../controllers/user-controller');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/')
    },
    filename: function(req, file, cb){
        cb(null, new Date().toISOString() + file.originalname)
    }
});

const fileFilter = (req, file, cb) =>{
    if( file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg'){

        cb(null, true);
    }else{
        cb(null, false); 
    }
}

const upload = multer({ 
    storage: storage,  
    limits:{
        fileSize: 1024 * 1024 * 5
    },
    fileFilter : fileFilter
} );

// Pesquisa usuario.
// router.get('/alluser', login.mandatory, UserConttoller.getUser);
router.get('/alluser', UserConttoller.getUser);
// Insert de usuario
router.post('/registration', (upload.single('photo')), UserConttoller.postUser);
// Retornar um usuario pesquisado
router.get('/:user_login', login.mandatory, UserConttoller.getUserDetail);
// Altera Usuario
router.patch('/', login.mandatory, (upload.single('photo')), UserConttoller.updateUser);
// Deleta Usuario.
 router.delete('/', login.mandatory, UserConttoller.deleteUser);

 router.post( '/login', UserConttoller.Login);

module.exports = router;
