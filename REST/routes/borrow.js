const express = require('express');
const router = express.Router();
const login = require('../middleware/login');

const BorrowConttoller = require('../controllers/borrow-controller');

// Retornar um usuario pesquisado
router.get('/:user_login', login.mandatory, BorrowConttoller.getBorrow);


//router.patch('/:borrow',  )

module.exports = router;
