const jwt = require('jsonwebtoken');

exports.mandatory  = (req, res, next) => {
    try{
        const token = req.headers.authorization.split(' ')[1];
        const decode = jwt.verify( token, 'Reuse2go-Luisa-Andre-Reuse2go');
        req.user = decode;
        next();
    }catch(error){
        
        return res.status(401).send( {message: 'Authentication failed' });
    }
}

exports.optional  = (req, res, next) => {
    try{
        const token = req.headers.authorization.split(' ')[1];
        const decode = jwt.verify( token, 'Reuse2go-Luisa-Andre-Reuse2go');
        req.user = decode;
        next();
    }catch(error){
        next();
    }
}