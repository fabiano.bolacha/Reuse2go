const express = require('express');

const mysql = require("../mysql");


exports.getBorrow = async (req, res, next) => {
    try{
        const sql = `SELECT 
        br.ID_BORROW
      , br.DT_BORROW
      , br.DT_BORROW_RETURN
      , br.STATUS
      , su.LOGIN
      , su.NAME AS NAME_USER
      , cr.ID_COMPANY
      , cr.NAME AS NAME_COMPANY
      , ibr.ID_ITEM
      , cor.ID_CONTAINER
      , cor.CONTAINER_VALUE
      , ccr.ID_CHARACTERISTIC
      , ccr.STYLE
      , ct.ID_CONTAINER_TYPE
      , ct.NAME_TYPE
      , cz.ID_SIZE
      , cz.NAME_SIZE
    FROM 
       php_reuse.borrow_reuse      br
     , php_reuse.sec_users         su
     , php_reuse.company_reuse     cr
     , php_reuse.item_borrow_reuse ibr
     , php_reuse.container_reuse   cor
     , php_reuse.container_characteristic_reuse ccr
     , php_reuse.container_size_reuse cz
     , php_reuse.container_type_reuse ct
     WHERE br.SEC_USERS_LOGIN_BORROW = ?
       AND su.login = br.SEC_USERS_LOGIN_BORROW
       AND cr.ID_COMPANY = br.ID_COMPANY_BORROW
       AND ibr.ID_BORROW_ITEM = br.ID_BORROW
       AND cor.ID_CONTAINER = ibr.ID_CONTAINER_ITEM
       AND ccr.ID_CHARACTERISTIC = cor.ID_CHARACTERISTIC_CONTAINER
       AND cz.ID_SIZE = ccr.ID_SIZE_CHARACTERISTIC
       AND ct.ID_CONTAINER_TYPE = ccr.ID_CONTAINER_TYPE_CHARACTERISTIC`;
        const result = await mysql.execute(sql, [req.params.user_login]) ;
       
        const response = {
        Quantity               : result.length                       ,
        borrow: result.map( borrow_return => {
            return {
                id_borrow              : borrow_return.ID_BORROW             , 
                dt_borrow              : borrow_return.DT_BORROW             , 
                dt_borrow_return       : borrow_return.DT_BORROW_RETURN      , 
                status                 : borrow_return.STATUS                ,
                user:{
                    login     : borrow_return.LOGIN     ,
                    name_user : borrow_return.NAME_USER              
                },
                company: {
                    id_company   : borrow_return.ID_COMPANY  ,
                    name_company : borrow_return.NAME_COMPANY          
                },
                item_borrow: {
                    id_item           : borrow_return.ID_ITEM           
                },
                container : {
                    id_container    : borrow_return.ID_CONTAINER,
                    container_value : borrow_return.CONTAINER_VALUE                 
                },
                container_characteristic: {
                    id_characteristic : borrow_return.ID_CHARACTERISTIC,
                    style             : borrow_return.STYLE                 
                },
                container_type: {
                    id_container_type : borrow_return.ID_CONTAINER_TYPE, 
                    name_type         : borrow_return.NAME_TYPE
                },
                container_size: {
                    id_size   : borrow_return.ID_SIZE,
                    name_size : borrow_return.NAME_SIZE 
                },
                
                   requet: {
                    type       : 'GET',
                    description: 'Returns all borrow for a customer',
                    url        : 'http://localhost:3000/user/' + req.params.user_login
                }
            }
        } )
    }

    return res.status(200).send(response)      

    }catch{
        return res.status(500).send({ error: error });
    }
}

