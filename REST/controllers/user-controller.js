const mysql     = require("../mysql");
const md5       = require('md5');
const base64Img = require('base64-img');
const jwt       = require('jsonwebtoken');


exports.getUser = async (req, res, next) => {
    try{
        const sql = 'select * from php_reuse.sec_users';
        const result = await mysql.execute( sql );

        const response = {
            Quantity: result.length,
            user: result.map( user_return => {
                return {
                    login                    :  user_return.login                  , 
                    name                     :  user_return.name                   , 
                    email                    :  user_return.email                  , 
                    visitor                  :  user_return.visitor                , 
                    active                   :  user_return.active                 , 
                    activation_code          :  user_return.activation_code        , 
                    priv_admin               :  user_return.priv_admin             , 
                    photo                    :  user_return.photo                  , 
                    accept_terms_conditions  :  user_return.accept_terms_conditions, 
                    address_1                :  user_return.address_1              , 
                    address_2                :  user_return.address_2              , 
                    name_building            :  user_return.name_building          , 
                    city                     :  user_return.city                   , 
                    postcode                 :  user_return.postcode               ,
                    requet: {
                        type       : 'GET',
                        description: 'Return user detail',
                        url        : 'http://localhost:3000/user/user_login/' + user_return.login
                    }
                }
            } )
        }

        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }   
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
exports.postUser = async (req, res, next) => {
    //   console.log(req.file);
    try{

        const sql_consult = 'select * from php_reuse.sec_users where login = ?';
        const result_consult = await mysql.execute( sql_consult, [ req.body.login ] );
     
        if( result_consult.length > 0 ){
            return res.status(400).send({ message : 'This login already exists' });
        }

         const user = {
            login                   : req.body.login,
            pswd                    : md5(req.body.pswd),
            email                   : req.body.email,
            visitor                 : req.body.visitor,
            active                  : req.body.active,
            photo                   : req.file.path,
            accept_terms_conditions : req.body.accept_terms_conditions,
        };

        const sql = `INSERT INTO php_reuse.sec_users 
                    (
                        login                   ,
                        pswd                    ,
                        email                   ,
                        visitor                 ,
                        active                  ,
                        photo                   ,
                        accept_terms_conditions 
                    ) 
                    VALUES  (?,?,?,?,?,?,?)`;

        const result = await mysql.execute( 
            sql, 
            [
                user.login, 
                user.pswd, 
                user.email,
                user.visitor, 
                user.active, 
                base64Img.base64Sync(user.photo), 
                user.accept_terms_conditions 
            ]
            );

            const response = {
                message  : 'User successfully entered' ,
                userCriate: {
                    login                    :  user.login                  , 
                    email                    :  user.email                  , 
                    visitor                  :  user.visitor                , 
                    active                   :  user.active                 , 
                    photo                    :  user.photo                  , 
                    accept_terms_conditions  :  user.accept_terms_conditions, 
                    requet: {
                        type       : 'POST',
                        description: 'Inserting a user',
                        url        : 'http://localhost:3000/user/alluser'
                    }
                } 
            }// fim response

        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }   
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
exports.getUserDetail = async (req, res, next) => {
    try{
        
        const sql = 'select * from php_reuse.sec_users where login = ?';
        const result = await mysql.execute( sql, [req.params.user_login] );
        const response = {
            user: {
                login                    :  result[0].login                  , 
                name                     :  result[0].name                   , 
                email                    :  result[0].email                  , 
                visitor                  :  result[0].visitor                , 
                active                   :  result[0].active                 , 
                activation_code          :  result[0].activation_code        , 
                priv_admin               :  result[0].priv_admin             , 
                photo                    :  result[0].photo                  , 
                accept_terms_conditions  :  result[0].accept_terms_conditions, 
                address_1                :  result[0].address_1              , 
                address_2                :  result[0].address_2              , 
                name_building            :  result[0].name_building          , 
                city                     :  result[0].city                   , 
                postcode                 :  result[0].postcode               ,
                requet: {
                    type       : 'GET',
                    description: 'Return from all users',
                    url        : 'http://localhost:3000/user/'
                }
            } // Fim user
        }

        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }   
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
exports.updateUser = async (req, res, next) => {
    try{
        const sql_consult = 'select * from php_reuse.sec_users where login = ?';
        const result_consult = await mysql.execute( sql_consult, [ req.body.login ] );
     
        if( result_consult.length == 0 ){
            return res.status(400).send({ message : 'Login not found' });
        }
        
        const sql = `UPDATE php_reuse.sec_users 
                        SET pswd                    = ?,
                            name                    = ?,
                            email                   = ?,
                            visitor                 = ?,
                            active                  = ?,
                            activation_code         = ?,
                            photo                   = ?,
                            accept_terms_conditions = ?,
                            address_1               = ?,
                            address_2               = ?,
                            name_building           = ?,
                            city                    = ?,
                            postcode                = ?
                    WHERE login = ?`;


        await mysql.execute( sql, 
                            [
                                md5(req.body.pswd),
                                req.body.name,
                                req.body.email,
                                req.body.visitor,
                                req.body.active,
                                req.body.activation_code,
                                base64Img.base64Sync(req.file.path),
                                req.body.accept_terms_conditions,
                                req.body.address_1,
                                req.body.address_2,
                                req.body.name_building,
                                req.body.city,
                                req.body.postcode,
                                req.body.login
                            ] 
                            );
        console.log('Passei 3');
        const response = {
            mensagem  : 'User successfully Update' ,
            user: {
                login                    :  req.body.login                  , 
                name                     :  req.body.name                   , 
                email                    :  req.body.email                  , 
                visitor                  :  req.body.visitor                , 
                active                   :  req.body.active                 , 
                activation_code          :  req.body.activation_code        , 
                priv_admin               :  req.body.priv_admin             , 
                photo                    :  req.body.photo                  , 
                accept_terms_conditions  :  req.body.accept_terms_conditions, 
                address_1                :  req.body.address_1              , 
                address_2                :  req.body.address_2              , 
                name_building            :  req.body.name_building          , 
                city                     :  req.body.city                   , 
                postcode                 :  req.body.postcode               ,
                requet: {
                    type       : 'POST',
                    description: 'User Update',
                    url        : 'http://localhost:3000/user/user_login/' + req.body.login
                }
            } // Fim user
        };
        console.log('Passei 4');                    
        return res.status(200).send(response);     

    }
    catch{
        console.log('Passei erro: ' );
        return res.status(500).send({ error: error });
    }   
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
exports.deleteUser = async (req, res, next) => {
    try{
        const sql_user = 'select * from php_reuse.sec_users where login = ?';
        const result_user = await mysql.execute( sql_user, [ req.body.login ] );
     
        if( user.length == 0 ){
            return res.status(400).send({ message : 'Login not found' });
        }

        const sql_consult = 'select * from php_reuse.users_has_company where users_login = ?';
        const result_consult = await mysql.execute( sql_consult, [ req.body.login ] );
     
        if( result_consult.length > 0 ){
            const sql_delete = 'DELETE FROM php_reuse.users_has_company where users_login = ?';
            await mysql.execute( sql_delete, [ req.body.login ]  );
        }

        const sql = 'DELETE FROM php_reuse.sec_users WHERE login = ?';
        await mysql.execute(sql, [ req.body.login ] );

        const response = {
            mensagem: 'User successfully Delete',
            requet: {
                type       : 'POST',
                description: 'delete a user ',
                url        : 'http://localhost:3000/user/alluser'
            }
        }

        return res.status(200).send(response);     

    }catch{
        console.error(error);
        return res.status(500).send({ error: error });
    }   
};

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
exports.Login = async (req, res, next) => {
    try{

        const sql = 'SELECT * FROM php_reuse.sec_users WHERE login = ?';
        const result = await mysql.execute( sql, [ req.body.login ] );
     
        if( result.length < 1 ){
            return res.status(404).send( {message: 'User not found' });
        }

        const ps       = result[0].pswd;
        const pswd_md5 = md5(req.body.password);
        if(ps != pswd_md5 ){
            return res.status(401).send( {message: 'Authentication failed',});
        }
        if(result){
            const token = jwt.sign({
                login  : result[0].login,
                email  : result[0].email,
                visitor: result[0].visitor
            }, 'Reuse2go-Luisa-Andre-Reuse2go',
            {
                expiresIn: "1h"
            });
            return res.status(200).send( {
                message: 'successful authentication',
                login: result[0].login,
                token: token
            });
        }

        return res.status(401).send( {message: 'Authentication failed' });     

    }catch{
        console.error(error);
        return res.status(500).send({ error: error });
    }   
};
