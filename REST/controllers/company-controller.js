const mysql = require("../mysql");

exports.getCompany = async(req, res, next) => {
    try{
        const sql = 'select * from company_reuse';

        const result = await mysql.execute(sql) ;

        const response = {
            Quantity: result.length,
            company : result.map( company_return => {
                return {
                    id_company     : company_return.ID_COMPANY     ,
                    name           :  company_return.NAME          ,
                    email          :  company_return.EMAIL         , 
                    company_number :  company_return.COMPANY_NUMBER, 
                    dt_of_creation :  company_return.DT_OF_CREATION, 
                    address_1      :  company_return.ADDRESS_1     , 
                    address_2      :  company_return.ADDRESS_2     , 
                    name_building  :  company_return.NAME_BUILDING , 
                    city           :  company_return.CITY          , 
                    postcode       :  company_return.POSTCODE      , 
                    note           :  company_return.NOTE          , 

                    requet: {
                        type       : 'GET',
                        description: 'Return Company detail',
                        url        : 'http://localhost:3000/company/' + company_return.ID_COMPANY
                    }
                }
            })
        }
        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }
}


exports.getCompanyDetail = async (req, res, next) => {
    try{

        const sql_company = 'SELECT * FROM php_reuse.company_reuse WHERE id_company = ?';
        const result_company = await mysql.execute( sql_company, [ req.params.id_company ] );
     
        if( result_company.length < 1 ){
            return res.status(404).send( {message: 'Company not found' });
        }

        const sql = 'select * from company_reuse where id_company = ?';
        const result = await mysql.execute(sql, [req.params.id_company]) ;

        const response = {
            company: {
                id_company     : result[0].ID_COMPANY    ,
                name           : result[0].NAME          ,
                email          : result[0].EMAIL         ,
                company_number : result[0].COMPANY_NUMBER,
                dt_of_creation : result[0].DT_OF_CREATION,
                address_1      : result[0].ADDRESS_1     ,
                address_2      : result[0].ADDRESS_2     ,
                name_building  : result[0].NAME_BUILDING ,
                city           : result[0].CITY          ,
                postcode       : result[0].POSTCODE      ,
                note           : result[0].NOTE          ,
                requet: {
                    type       : 'GET',
                    description: 'Return from all Company',
                    url        : 'http://localhost:3000/company/'
                }
            } // Fim user
        }
        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }   
}
