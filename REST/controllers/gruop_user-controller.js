const mysql = require("../mysql");


exports.postGroupUser = async (req, res, next) => {
    try{

        const users_groups = {
            login    : req.body.login,
            group_id : req.body.group_id
        };

        if( req.body.login != 2 ) {
            users_groups.group_id = 2;
        }

        const sql_consult = `select * 
                              from php_reuse.sec_users_groups
                             where login    = ? 
                               and group_id = ?`;
        const result_consult = await mysql.execute( sql_consult, [ users_groups.login, users_groups.group_id ] );
     
        if( result_consult.length > 0 ){
            return res.status(405).send({ message : 'This information already exists registered for the user' });
        }

        const sql = 'INSERT INTO sec_users_groups( login, group_id ) VALUES ( ?, ? )';
        await mysql.execute(sql, [ users_groups.login, users_groups.group_id ] );

        const response = {
            message  : 'Group User successfully entered' ,
            GroupuserCriate: {
                login   : users_groups.login, 
                group_id: users_groups.group_id,

                requet: {
                    type       : 'POST',
                    description: 'Inserting a Group user',
                    url        : 'http://localhost:3000/user/'
                }
            } 
        }// fim response
        
        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }   
}

