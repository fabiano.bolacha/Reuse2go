const mysql = require("../mysql");

exports.postUserCompany = async (req, res, next) => {
    try{
        const sql_consult = `select * 
                               from php_reuse.users_has_company 
                               where login      = ?
                                 and id_company = ?`;
        const result_consult = await mysql.execute( sql_consult, [ req.body.login, req.body.id_company ] );
     
        if( result_consult.length == 0 ){
            return res.status(405).send({ message : 'This information already exists registered for the user' });
        }

        const sql = 'INSERT INTO users_has_company( users_login, id_company ) VALUES ( ?, ? )';
        await mysql.execute(sql, [ req.body.login, req.body.id_company ] );

        const response = {
            message  : 'User, Company successfully entered' ,
            GroupuserCriate: {
                login      : req.body.login,
                id_company : req.body.id_company,
                requet: {
                    type       : 'POST',
                    description: 'Inserting a User, Company',
                    url        : 'http://localhost:3000/user/'
                }
            } 
        }// fim response

        return res.status(200).send(response);     

    }catch{
        return res.status(500).send({ error: error });
    }   
}

